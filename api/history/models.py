from django.db import models

# Patient model !!
class Patient(models.Model):
    name = models.CharField(max_length=255)
    age = models.PositiveIntegerField()
    state = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return self.name

# Medical History Model !!
class MedicalHistory(models.Model):
    patient = models.ForeignKey(Patient, on_delete=models.CASCADE)
    treatment_name = models.CharField(max_length=255)
    doctor_name = models.CharField(max_length=255)
    hospital_name = models.CharField(max_length=255)
    cost = models.DecimalField(max_digits=10, decimal_places=2)
    medication = models.CharField(max_length=255)
    treatment_date = models.DateTimeField()

    def __str__(self):
        return self.treatment_name

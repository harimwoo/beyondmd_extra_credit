from django.shortcuts import render, redirect, get_object_or_404
from history.models import Patient, MedicalHistory
from history.forms import PatientForm, MedicalHistoryForm
import requests, json
import random
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

def list_patients_json(request):
    patients = Patient.objects.all()
    data = [{"id": patient.id, "name": patient.name, "age": patient.age, "state": patient.state, "phone_number": patient.phone_number} for patient in patients]
    return JsonResponse(data, safe=False)

@csrf_exempt
def create_or_edit_patient_json(request, patient_id=None):
    if request.method == "POST":
        try:
            data = json.loads(request.body)
            if patient_id:
                # Editing an existing patient
                patient = get_object_or_404(Patient, id=patient_id)
                form = PatientForm(data, instance=patient)
            else:
                # Creating a new patient
                form = PatientForm(data)

            if form.is_valid():
                form.save()
                return JsonResponse({"success": True})
            else:
                errors = form.errors
                return JsonResponse({"success": False, "errors": errors})
        except json.JSONDecodeError as e:
            return JsonResponse({"error": "Invalid JSON data", "details": str(e)}, status=400)

    # Return an error response for GET requests (if not handled differently)
    return JsonResponse({"error": "Invalid request method"}, status=405)

def create_patient(request):
    if request.method == "POST":
        form = PatientForm(request.POST) # using awesome django form !!
        if form.is_valid():
            form.save() # save the form !!
            return redirect("show_patients") # redirect to patient list page !!
    else:
        form = PatientForm()
    context = {"form": form} # assigning obj to context !!
    return render(request, "patients/create_patient.html", context) # feeding the context to HTML template !!




@csrf_exempt
def create_treatment(request, patient_id):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            form = MedicalHistoryForm(data)
            if form.is_valid():
                treatment = form.save(commit=True)  # Use commit=True to immediately save the treatment
                treatment.patient_id = patient_id  # Set the patient ID
                treatment.save()
                return JsonResponse({'message': 'Treatment created successfully'})
            else:
                errors = form.errors
                return JsonResponse({'errors': errors}, status=400)
        except json.JSONDecodeError as e:
            return JsonResponse({'error': 'Invalid JSON data', 'details': str(e)}, status=400)
    else:
        return JsonResponse({'message': 'Method not allowed'}, status=405)

def detail_patient(request, patient_id):
    patient = get_object_or_404(Patient, id=patient_id)
    treatments = MedicalHistory.objects.filter(patient=patient_id)

    patient_data = {
        'id': patient.id,
        'name': patient.name,
        'age': patient.age,
        'state': patient.state,
        'phone_number': patient.phone_number,
    }

    treatments_data = []
    if treatments.exists():
        for treatment in treatments:
            treatments_data.append({
                'id': treatment.id,
                'treatment_name': treatment.treatment_name,
                'doctor_name': treatment.doctor_name,
                'hospital_name': treatment.hospital_name,
                'cost': treatment.cost,
                'medication': treatment.medication,
                'treatment_date': treatment.treatment_date.strftime('%Y-%m-%d'),  # Format the date as needed
            })

    data = {
        'patient': patient_data,
        'treatments': treatments_data,
    }

    return JsonResponse(data)


# View function for updating patient !!
def update_patient(request, id):
    patient = get_object_or_404(Patient, id=id)

    if request.method == "POST":
        form = PatientForm(request.POST, instance=patient)
        if form.is_valid():
            form.save()
            return redirect("detail_patient", id=id)
    else:
        form = PatientForm(instance=patient)

    context = {"form": form, "patient": patient}
    return render(request, "patients/update_patient.html", context)


@csrf_exempt
def delete_patient(request, patient_id):
    try:
        patient = Patient.objects.get(id=patient_id)
        patient.delete()
        return JsonResponse({'success': True})
    except Patient.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'Patient not found'})


@csrf_exempt
def delete_treatment(request, treatment_id):
    try:
        treatment = MedicalHistory.objects.get(id=treatment_id)
        treatment.delete()
        return JsonResponse({'success': True})
    except MedicalHistory.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'Treatment not found'})

# View function for getting a random article !!
def random_article(request):
    url = 'https://www.healthcare.gov/api/articles.json'
    response = requests.get(url)

    if response.status_code == 200:
        try:
            data = response.json()
            articles = data.get('articles', [])

            # Check if it returned at least 5 articles
            if len(articles) >= 5:
                random_article = random.choice(articles)
            else:
                random_article = None
        except json.JSONDecodeError as e:
            print(f"JSON decoding error: {str(e)}")
            random_article = None
    else:
        random_article = None

    # Return JSON response instead of rendering HTML
    return JsonResponse({"random_article": random_article})

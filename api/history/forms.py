from django.forms import ModelForm
from history.models import Patient, MedicalHistory

# create forms for django to be used for CRUD functionality !!
class PatientForm(ModelForm):
    class Meta:
        model = Patient
        fields = ["name", "age", "state", "phone_number"]

class MedicalHistoryForm(ModelForm):
    class Meta:
        model = MedicalHistory
        fields = ["patient", "treatment_name", "doctor_name", "hospital_name", "cost", "medication", "treatment_date"]

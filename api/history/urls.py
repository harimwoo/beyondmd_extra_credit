from django.urls import path
# import view functions
from history.views import (
    update_patient,
    delete_patient,
    random_article,
    list_patients_json,
    create_or_edit_patient_json,
    delete_patient,
    detail_patient,
    create_treatment,
    delete_treatment
)

urlpatterns = [
    path("<int:id>/update/", update_patient, name="update_patient"),
    path("<int:id>/delete/", delete_patient, name="delete_patient"),
    path("random_article/", random_article, name="random_article"),
    path('api/patients/', list_patients_json, name='list_patients_json'),
    path('api/create/', create_or_edit_patient_json, name='create_patient_json'),
    path('api/edit/<int:patient_id>/', create_or_edit_patient_json, name='edit_patient_json'),
    path('api/delete/<int:patient_id>/', delete_patient, name='delete_patient'),
    path('api/detail_patient/<int:patient_id>/', detail_patient, name='detail_patient'),
    path('api/create_treatment/<int:patient_id>/', create_treatment, name='create_treatment'),
    path('api/delete_treatment/<int:treatment_id>/', delete_treatment, name='delete_treatment'),
]

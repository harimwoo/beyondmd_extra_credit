import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Nav from './components/Nav';
import Footer from './components/Footer';
import PatientList from './components/PatientList';
import { OpenResume } from './components/OpenResume';
import Article from './components/Article';
import { CssBaseline } from '@mui/material';
import PatientDetails from './components/PatientDetails';
import { useParams } from 'react-router-dom';
import CreateTreatment from './components/CreateTreatment';
import ListRecord from './components/ListRecord';

function PatientDetailsPage() {
    const { patientId } = useParams();

    return <PatientDetails patientId={patientId} />;
}

function CreateTreatmentPage() {
  const { patientId } = useParams();

  return <CreateTreatment patientId={patientId} />;
}

const App = () => {
	return (
		<div>
			<CssBaseline>
				<Nav/>
				<Routes>
					<Route path="/patient/:patientId" element={<PatientDetailsPage />} />
					<Route path="/patients" element={<PatientList />}/>
					<Route path="/resume" element={<OpenResume />}/>
					<Route path="/article" element={<Article />}/>
					<Route path="/create_treatment/:patientId" element={<CreateTreatmentPage />} />
					<Route path="/" element={<ListRecord />}/>
				</Routes>
				<Footer />
			</CssBaseline>
		</div>
	);
};

export default App;

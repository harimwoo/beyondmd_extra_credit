import React from 'react';
import { AppBar, Link, Typography } from '@mui/material';

const Footer = () => {
  const headingStyle = {
    fontSize: '16px',
    color: 'rgb(15, 164, 231)',
  };

  return (
    <AppBar
      position="static"
      sx={{
        position: 'absolute',
        bottom: 0,
        background: '#00254B',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: '20px 40px',
      }}
    >
		<div>
		<a href="http://localhost:3000">
			<img
				src="/beyondCircle.png"
				alt="beyondMD logo"
				height="50px"
			/>
        </a>

		</div>

      <div style={{ marginLeft: '20px', marginRight: '20px' }}>
		<Typography variant="subtitle2" sx={headingStyle}>
          BeyondMD
        </Typography>
        <Typography variant="subtitle2">
          <Link href="http://beyondmd.care" color="inherit" target="_blank">
            About Us
          </Link>
        </Typography>
        <Typography variant="subtitle2">
          <Link href="https://calendly.com/alexmol" color="inherit" target="_blank">
            Schedule Consultation
          </Link>
        </Typography>
      </div>

      <div style={{ marginLeft: '20px', marginRight: '20px' }}>
        <Typography variant="subtitle2" sx={headingStyle}>
          Contact
        </Typography>
        <Typography variant="subtitle2">
          Email: harimwoo@gmail.com
        </Typography>
        <Typography variant="subtitle2">
          Phone: (470) 408-1354
        </Typography>
      </div>

      <div>
        <Typography variant="subtitle2" sx={headingStyle}>
          Social
        </Typography>
		<a href="http://linkedin.com/in/harimwoo/">
			<img
				src="/linkedin.png"
				alt="linedkin logo"
				height="40px"
			/>
        </a>
      </div>
    </AppBar>
  );
};

export default Footer;

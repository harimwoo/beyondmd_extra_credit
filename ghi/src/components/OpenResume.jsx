import React from 'react';
import { Box } from '@mui/material';
import { Document, Page, pdfjs } from 'react-pdf';

export function OpenResume() {
  pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

  const containerStyles = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '80vh',
    fontFamily: 'Arial',
    fontSize: '1.2rem',
  };

  const headingStyles = {
    marginTop: '20px',
    fontFamily: 'Arial',
    fontSize: '25px',
  };

  const documentStyles = {
    maxWidth: '80%', // Set the maximum width of the document
    maxHeight: '80vh', // Set the maximum height of the document
    overflow: 'auto', // Add scrolling if the content exceeds the maximum height
  };

  return (
    <div style={containerStyles}>
      <h2 style={headingStyles}>Resume</h2>
      <Box style={documentStyles}>
        <Document file="resume.pdf">
          <Page
            className="pdf-page"
            pageNumber={1}
            renderTextLayer={false}
            renderAnnotationLayer={false}
            width={700}
          />
        </Document>
      </Box>
    </div>
  );
}

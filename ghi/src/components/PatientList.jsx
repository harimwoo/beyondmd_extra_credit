import React, { useState, useEffect } from 'react';
import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material';
import dayjs from 'dayjs';

function PatientList() {
  const [patientInfo, setPatientInfo] = useState([]);
  useEffect(() => {
    const fetchData = async () => {
      const url = 'https://randomuser.me/api/?results=8';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setPatientInfo(data.results);
      } else {
        console.error("Error fetching patient info: ", await response.text());
      }
    }
    fetchData();
  }, []);

  const containerStyles = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    height: '80vh',
    fontFamily: 'Arial',
    fontSize: '1.2rem',
  };

  const headingStyles = {
  marginTop: '20px',
  fontFamily: 'Arial',
  fontSize: '25px'
  };

  const tableContainerStyles = {
    width: '80%',
    margin: '20px',
  };

  const tableHeaderCellStyles = {
    fontWeight: 'bold',
    textAlign: 'center',
    background: 'rgb(39, 142, 238)', // Shading for the table header
    color: 'white',
  };

  const tableRowStyles = {
    '&:nth-of-type(odd)': {
      backgroundColor: 'rgb(193, 237, 238)', // Light sky blue for odd rows
    },
  };

  return (
    <div style={containerStyles}>
      <h2 style={headingStyles}>Patient List</h2>
      <TableContainer component={Paper} style={tableContainerStyles}>
        <Table aria-label="patient-info-table">
          <TableHead>
            <TableRow>
              <TableCell style={tableHeaderCellStyles}>Name</TableCell>
              <TableCell style={tableHeaderCellStyles}>Email</TableCell>
              <TableCell style={tableHeaderCellStyles}>Gender</TableCell>
              <TableCell style={tableHeaderCellStyles}>Age</TableCell>
              <TableCell style={tableHeaderCellStyles}>DOB</TableCell>
              <TableCell style={tableHeaderCellStyles}>Nationality</TableCell>
              <TableCell style={tableHeaderCellStyles}>Location</TableCell>
              <TableCell style={tableHeaderCellStyles}>Photo</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {patientInfo.map((patient, index) => (
              <TableRow key={index} style={tableRowStyles}>
                <TableCell>{`${patient.name.title}. ${patient.name.first} ${patient.name.last}`}</TableCell>
                <TableCell>{patient.email}</TableCell>
                <TableCell>{patient.gender}</TableCell>
                <TableCell>{patient.dob.age}</TableCell>
                <TableCell>{dayjs(patient.dob.date).format('YYYY-MM-DD')}</TableCell>
                <TableCell>{patient.nat}</TableCell>
                <TableCell>{`${patient.location.city}, ${patient.location.state}, ${patient.location.country}`}</TableCell>
                <TableCell>
                  <img
                    src={patient.picture.large}
                    alt={`${patient.name.first}'s photo`}
                    width="100"
                    height="100"
                  />
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default PatientList;

import React, { useState } from 'react';
import {  Paper } from '@mui/material';

export default function Article() {
  const [randomArticle, setRandomArticle] = useState(null);

  function createMarkup(html) {
    return { __html: html };
  }

  const containerStyles = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    maxWidth: '80%',
    margin: '0 auto',
    paddingTop: '30px',
    fontFamily: 'Arial',
    fontSize: '1.2rem',
  };

  const articleStyles = {
    maxHeight: '70vh',
    overflowY: 'auto',
    width: '100%',
    padding: '20px',
  };

  const headingStyles = {
    marginTop: '20px',
    fontFamily: 'Arial',
    fontSize: '25px'
  };

  const fetchRandomArticle = async () => {
    try {
      const response = await fetch('http://localhost:8000/patients/random_article/');
      if (response.ok) {
        const data = await response.json();
        setRandomArticle(data.random_article);
      } else {
        console.error('Failed to fetch random article');
      }
    } catch (error) {
      console.error('Error:', error);
    }
  };

  return (
    <div style={containerStyles}>
      <h2 style={headingStyles}>Articles from HealthCare.gov</h2>
      <button onClick={fetchRandomArticle}>Press for more</button>
      {randomArticle && (
        <Paper elevation={3} style={articleStyles}>
          <h2>{randomArticle.title}</h2>
          <div dangerouslySetInnerHTML={createMarkup(randomArticle.content)} />
        </Paper>
      )}
    </div>
  );
}

import React, { useState } from 'react';

const inputStyle = {
    marginBottom: '10px', // Add spacing between each input
};

function CreatePatientForm({ onPatientCreated }) {
    const [formData, setFormData] = useState({
        name: '',
        age: '',
        state: '',
        phone_number: '',
    });

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        fetch('http://localhost:8000/patients/api/create/', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(formData),
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    onPatientCreated(data);

                } else {
                    console.error('Error:', data);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    };



    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div style={inputStyle}>
                    <label htmlFor="name">Name:</label>
                    <input
                        type="text"
                        id="name"
                        name="name"
                        value={formData.name}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div style={inputStyle}>
                    <label htmlFor="age">Age:</label>
                    <input
                        type="number"
                        id="age"
                        name="age"
                        value={formData.age}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div style={inputStyle}>
                    <label htmlFor="state">State:</label>
                    <input
                        type="text"
                        id="state"
                        name="state"
                        value={formData.state}
                        onChange={handleChange}
                        required
                    />
                </div>

                <div style={inputStyle}>
                    <label htmlFor="phone_number">Phone Number:</label>
                    <input
                        type="text"
                        id="phone_number"
                        name="phone_number"
                        value={formData.phone_number}
                        onChange={handleChange}
                        required
                    />
                </div>

                <button type="submit">Create</button>
            </form>
        </div>
    );
}

export default CreatePatientForm;

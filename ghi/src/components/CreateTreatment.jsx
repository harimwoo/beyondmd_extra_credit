import React, { useState } from 'react';
import { useParams } from 'react-router-dom';

function CreateTreatment() {
    const { patientId } = useParams();

    const [formData, setFormData] = useState({
        treatment_name: '',
        doctor_name: '',
        hospital_name: '',
        cost: '',
        medication: '',
        treatment_date: '',
    });

    const handleInputChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        const newTreatment = {
            patient: patientId,
            treatment_name: formData.treatment_name,
            doctor_name: formData.doctor_name,
            hospital_name: formData.hospital_name,
            cost: formData.cost,
            medication: formData.medication,
            treatment_date: formData.treatment_date,
        };

        fetch(`http://localhost:8000/patients/api/create_treatment/${patientId}/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newTreatment),
        })
        .then((response) => response.json())
        .then((data) => {
            console.log('Treatment created:', data);
            // Optionally, you can redirect to the patient detail page after creating the treatment
            window.location.href = `/patient/${patientId}`;
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Treatment:</label>
                    <input
                        type="text"
                        name="treatment_name"
                        value={formData.treatment_name}
                        onChange={handleInputChange}
                    />
                </div>
                <div>
                    <label>Doctor:</label>
                    <input
                        type="text"
                        name="doctor_name"
                        value={formData.doctor_name}
                        onChange={handleInputChange}
                    />
                </div>
                <div>
                    <label>Hospital:</label>
                    <input
                        type="text"
                        name="hospital_name"
                        value={formData.hospital_name}
                        onChange={handleInputChange}
                    />
                </div>
                <div>
                    <label>Cost:</label>
                    <input
                        type="text"
                        name="cost"
                        value={formData.cost}
                        onChange={handleInputChange}
                    />
                </div>
                <div>
                    <label>Medication:</label>
                    <input
                        type="text"
                        name="medication"
                        value={formData.medication}
                        onChange={handleInputChange}
                    />
                </div>
                <div>
                    <label>Treatment Date:</label>
                    <input
                        type="date"
                        name="treatment_date"
                        value={formData.treatment_date}
                        onChange={handleInputChange}
                    />
                </div>
                <button type="submit">Create</button>
            </form>
        </div>
    );
}

export default CreateTreatment;

import React, { useState, useEffect } from 'react';
import CreateTreatment from './CreateTreatment';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    SvgIcon,
    Dialog,
    DialogTitle,
    DialogContent,
} from '@mui/material';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';

function PatientDetails({ patientId }) {
    const [patientData, setPatientData] = useState(null);
    const [openDialog, setOpenDialog] = useState(false);

    const openTreatmentDialog = () => {
        setOpenDialog(true);
    };

    const closeTreatmentDialog = () => {
        setOpenDialog(false);
    };

    const handleDeleteTreatment = (treatmentId) => {
        // Send a DELETE request to your Django backend for deleting the treatment
        fetch(`http://localhost:8000/patients/api/delete_treatment/${treatmentId}/`, {
            method: 'DELETE',
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.success) {
                // Reload the patient data to refresh the treatments list
                fetch(`http://localhost:8000/patients/api/detail_patient/${patientId}`)
                    .then((response) => response.json())
                    .then((data) => setPatientData(data))
                    .catch((error) => console.error('Error:', error));
            } else {
                console.error('Error:', data);
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    };

    useEffect(() => {
        fetch(`http://localhost:8000/patients/api/detail_patient/${patientId}`)
            .then((response) => response.json())
            .then((data) => setPatientData(data))
            .catch((error) => console.error('Error:', error));
    }, [patientId]);

    if (!patientData) {
        return <p>Loading...</p>;
    }

    return (
        <div>
            <h2 style={{ marginTop: '20px', marginLeft: 20 }}>{patientData.patient.name}</h2>
            <p style={{  marginLeft: 32 }}><strong>Age:</strong> {patientData.patient.age}</p>
            <p style={{ marginLeft: 32 }}><strong>State:</strong> {patientData.patient.state}</p>
            <p style={{  marginLeft: 32 }}><strong>Phone Number:</strong> {patientData.patient.phone_number}</p>
            <h2>Treatments</h2>
            <Button variant="outlined" onClick={openTreatmentDialog} style={{ marginTop: 5, marginLeft: 20, marginBottom: 10 }}>
                Create Treatment
            </Button>
            {patientData.treatments.length > 0 ? (
                <TableContainer component={Paper}>
                    <Table>
                        <TableHead>
                            <TableRow style={{ background: 'rgb(39, 142, 238)', color: 'white' }}>
                                <TableCell>Treatment Name</TableCell>
                                <TableCell>Doctor Name</TableCell>
                                <TableCell>Hospital Name</TableCell>
                                <TableCell>Cost</TableCell>
                                <TableCell>Medication</TableCell>
                                <TableCell>Treatment Date</TableCell>
                                <TableCell style={{ background: 'rgb(39, 142, 238)' }}>Actions</TableCell> {/* Add a new column for the delete button */}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {patientData.treatments.map((treatment, index) => (
                                <TableRow key={index}>
                                    <TableCell>{treatment.treatment_name}</TableCell>
                                    <TableCell>{treatment.doctor_name}</TableCell>
                                    <TableCell>{treatment.hospital_name}</TableCell>
                                    <TableCell>{treatment.cost}</TableCell>
                                    <TableCell>{treatment.medication}</TableCell>
                                    <TableCell>{treatment.treatment_date}</TableCell>
                                    <TableCell>
                                        <Button onClick={() => handleDeleteTreatment(treatment.id)}>
                                            <SvgIcon>
                                                <DeleteOutlineOutlinedIcon />
                                            </SvgIcon>
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            ) : (
                <p>This patient has no treatment</p>
            )}

            <Dialog open={openDialog} onClose={closeTreatmentDialog}>
                <DialogTitle>Create New Treatment</DialogTitle>
                <DialogContent>
                    <CreateTreatment patientId={patientId} onClose={closeTreatmentDialog} />
                </DialogContent>
            </Dialog>
        </div>
    );
}

export default PatientDetails;

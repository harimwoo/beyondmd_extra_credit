import { Link } from 'react-router-dom';
import React from 'react';
import { AppBar, Box, Button, Toolbar, Typography } from '@mui/material';

const linkStyles = {
  textDecoration: 'none',
  color: 'black'
};

const buttonStyles = {
  backgroundColor: '#00B295',
  color: 'white',
  borderRadius: '20px',
  width: 'auto',
  height: '40px',
  padding: '8px 20px',
  marginLeft: '15px'
};

const Nav = () => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" sx={{ p: 2 }} style={{ background: '#fff' }}>
        <Toolbar>
          <a href="http://localhost:3000">
            <img src="/beyondMD.png" alt="beyondMD logo" height="30px" />
          </a>
          <div style={{ display: 'flex', marginLeft: 'auto' }}>
            <div style={{ marginTop: '8px' }}>
              <Typography variant="body1" color="textPrimary" sx={{ cursor: 'pointer', marginRight: 2 }}>
                <Link to="/" style={linkStyles}>
                  Medical Records
                </Link>
              </Typography>
            </div>
            <div style={{ marginTop: '8px', marginLeft: 20 }}>
              <Typography variant="body1" color="textPrimary" sx={{ cursor: 'pointer', marginRight: 2 }}>
                <Link to="/resume" style={linkStyles}>
                  Resume
                </Link>
              </Typography>
            </div>
            <div style={{ marginTop: '8px', marginLeft: 20 }}>
              <Typography variant="body1" color="textPrimary" sx={{ cursor: 'pointer', marginRight: 2 }}>
                <Link to="/patients" style={linkStyles}>
                  Patient List
                </Link>
              </Typography>
            </div>
            <Link to="/article" style={linkStyles}>
              <Button style={buttonStyles}>Health Article</Button>
            </Link>
          </div>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default Nav;

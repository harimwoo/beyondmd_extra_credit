import React, { useState, useEffect } from 'react';
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    Button,
    SvgIcon,
    Dialog,
    DialogTitle,
    DialogContent,
} from '@mui/material';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';

// Import the CreatePatientForm component here
import CreatePatientForm from './CreatePatientForm';

const tableContainerStyle = {
    maxHeight: '50vh',
    overflow: 'auto',
    width: '100%', // Set the width to 100% to remove trailing space
};

const tableStyle = {
    tableLayout: 'fixed', // Fix the table layout
};

function ListRecord() {
    const [patients, setPatients] = useState([]);
    const [openDialog, setOpenDialog] = useState(false);

    const openCreatePatientDialog = () => {
        setOpenDialog(true);
    };

    const closeCreatePatientDialog = () => {
        setOpenDialog(false);
    };

    const handleDelete = (patientId) => {
        // Send a DELETE request to your Django backend
        fetch(`http://localhost:8000/patients/api/delete/${patientId}`, {
            method: 'DELETE',
        })
            .then((response) => response.json())
            .then((data) => {
                if (data.success) {
                    // Update the list of patients by removing the deleted patient
                    setPatients(patients.filter((patient) => patient.id !== patientId));
                } else {
                    console.error('Error:', data);
                }
            })
            .catch((error) => {
                console.error('Error:', error);
            });
    };


    useEffect(() => {
        fetch('http://localhost:8000/patients/api/patients')
            .then((response) => response.json())
            .then((data) => setPatients(data))
            .catch((error) => console.error('Error:', error));
    });




    // Create a function to handle the creation of a new patient
    const handlePatientCreated = (newPatient) => {
        // Add the newly created patient to the list
        setPatients([...patients, newPatient]);
        closeCreatePatientDialog();
    };

    return (
        <div>
            <h2 style={{ marginTop: '20px', marginLeft: 20 }}>Patient List</h2>
            <Button variant="outlined" onClick={openCreatePatientDialog} style={{ marginTop: 5, marginLeft: 20, marginBottom: 10 }}>
                Create New Patient
            </Button>
            <TableContainer component={Paper} sx={tableContainerStyle}>
                <Table sx={tableStyle}>
                    <TableHead>
                        <TableRow style={{ background: 'rgb(39, 142, 238)', color: 'white' }}>
                            <TableCell>Name</TableCell>
                            <TableCell>Age</TableCell>
                            <TableCell>State</TableCell>
                            <TableCell>Phone Number</TableCell>
                            <TableCell>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {patients.map((patient) => (
                            <TableRow key={patient.id}>
                                <TableCell>{patient.name}</TableCell>
                                <TableCell>{patient.age}</TableCell>
                                <TableCell>{patient.state}</TableCell>
                                <TableCell>{patient.phone_number}</TableCell>
                                <TableCell>
                                    <Button variant="outlined" size="small" sx={{ mr: 1 }}>
                                        <a href={`/patient/${patient.id}`}>View Detail</a>
                                    </Button>
                                    <Button onClick={() => handleDelete(patient.id)}>
                                        <SvgIcon>
                                            <DeleteOutlineOutlinedIcon />
                                        </SvgIcon>
                                    </Button>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>

            <Dialog open={openDialog} onClose={closeCreatePatientDialog}>
                <DialogTitle>Create New Patient</DialogTitle>
                <DialogContent>
                    <CreatePatientForm onPatientCreated={handlePatientCreated} onClose={closeCreatePatientDialog} />
                </DialogContent>
            </Dialog>
        </div>
    );
}

export default ListRecord;
